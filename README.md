# ArmarX Academy

Welcome to the ArmarX Academy!

This will be the place where you can learn ArmarX, ask questions, report problems 
and exchange with others.
Here, you will find **tutorials**,
and a wiki-like knowledge base with **How-To's** and other references,
as well as a **Q&A issue** board for asking questions.

The academy is **currently under construction**, 
but feel free to explore while you're here.


## Quick Links (External)

- [**Axii (ArmarX Integrated Installer)**](https://gitlab.com/ArmarX/meta/setup) (see also: [Tutorial: Install ArmarX](tutorials/000_getting_started/001_install_armarx)) 
- [**General and API documentation**](https://armarx.humanoids.kit.edu/): https://armarx.humanoids.kit.edu/


## Navigation
 
- [**Tutorials**](tutorials): Learn ArmarX from ground up in a guided step-by-step manner.
- [**How To's**](how-tos): Smaller instructions and code snippets showing how to do specific things.
- [**Examples**](examples): 
  Collection of links to existing code snippets or example components 
  that you can take as inspiration or reference for your own project. 


## Asking Questions (Q&A)

Feel free to ask questions or report problems by 
**[creating an issue](https://gitlab.com/ArmarX/meta/Academy/-/issues)**.
Questions can be related to ArmarX in general or to the academy in specific.



# Contributing

## Tips on Editing Tutorials

- PyCharm (the Python IDE by JetBrains) is also great for editing Markdown. It provides a side-by-side preview, which also supports images.
  In addition, it has some refactoring capabilities which is useful when renaming directories, for example. 


## Contribute as a User / Student

As a user or student, you can contribute in several ways:

- Create an **issue**: If you have a question or want to report a problem, 
  e.g. an outdated tutorial, an unexplained error or a confusing explanation.
  Either wait for someone to respond, or directly mention (with `@username`) 
  your supervisor or someone else from the team.
- Create a **merge request**: If you already have an updated version, a fix for a problem,
  a suggestion for an explanation or other concrete changes, feel free to change the tutorials
  on a branch and create a merge reqeust. 
  We can then check whether we agree with these changes or suggest more changes.
  If everything fits, we can directly merge your changes into the main version.


## Contact

In any case, feel free to contact us directly. 
You can find our email addresses [on our team page](https://h2t.anthropomatik.kit.edu/21.php). 

- Rainer Kartmann
- Christian Dreher
