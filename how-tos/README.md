# How To's

Compared to the [tutorials](../tutorials), 
this section contains smaller instructions or code snippets 
showing how specific things can be done.

> See also the [How To's](https://armarx.humanoids.kit.edu/ArmarXDoc-HowTos.html)
> and the [FAQs](https://armarx.humanoids.kit.edu/ArmarXDoc-FAQ.html)
> in the ArmarX documentation.


**Table of Contents:**

[[_TOC_]]


**How to ...**
- [Set Up QtCreator for ArmarX](set-up-qtcreator-for-armarx)
- [Create an ArmarX Package in a Git Repository](create_an_armarx_package_in_a_git_repository)
- [Find the Name of an ArmarX Package](find_the_name_of_an_armarx_package)
- [Install and Activate CUDA / cuDNN](install_and_activate_cuda_cudnn)
- [Record Multiple Image Streams Simultaneously](record_multiple_image_streams_simultaneously)
- [Record Memory Data using the Long-Term Memory (LTM)](record_memory_data_using_the_long_term_memory)
- [View Images using PlaybackImageProvider](view_images_using_playback_image_provider)
- [Interactively Edit the Object Memory](interactively_edit_the_object_memory)
