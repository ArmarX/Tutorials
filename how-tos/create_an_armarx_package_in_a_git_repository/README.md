[[_TOC_]]


# How to Create an ArmarX Package in a Git Repository

The easiest way to create an ArmarX package in a Git repository is like this:

1. Create the Git repository (e.g. by creating a project on a platform such as Gitlab.com). 
   If you are a student at H2T, your supervisor will probably set this up for you.
   Let's call the project **"awesome_robotics"**.

2. Clone the (potentially empty) repository to a location of your choice, e.g. inside your ArmarX workspace:
    ```shell
    # Clone the git repository.
    git clone url/to/awesome_robotics.git  
    ```
   Assume this clones the project to the directory `awesome_robotics`.

3. Go into the directory and (either on the top-level or in a subdirectory) 
   create the package using the package tool:
    ```shell
    # Go into the cloned Git repository.
    cd awesome_robotics  
    # Create the ArmarX package.
    armarx-package init awesome_robotics  
    ```
   Note that this creates another directory `awesome_robotics` containing the package files.
   That is, the file system structure now looks like this:
    ```shell
    awesome_robotics/     # The Git repository. 
      awesome_robotics/   # The ArmarX package.
        README.md
        CMakeLists.txt
        build/
        source/
        ...
    ```

4. **_If_** you want the repository to directly contain the package files
   (i.e. `README.md` and `CMakeLists.txt` in the top-level directory of the repository),
   move all files and directories (including hidden ones starting with a `.`) 
   from the created package to the repository, then remove the directory:
    ```shell
    # In the Git repository:
    mv awesome_robotics/* awesome_robotics/.* .
    rmdir awesome_robotics
    ```
   > You may get these errors:
   > ```shell
   > mv: cannot move 'awesome_robotics/.' to './.': Device or resource busy 
   > mv: cannot move 'awesome_robotics/..' to './..': Device or resource busy
   > ```
   > That is ok. This is just a side effect of `.` and `..` being part of `.*`.

    Now the repositoriy should look like this:
    ```shell
    awesome_robotics/  # The Git repository AND the ArmarX package.
      README.md
      CMakeLists.txt
      build/
      source/
      ...
    ```
   
5. Commit the files to Git:
    ```shell
    git add .
    git add --all build/.gitkeep
    git commit -m "Create ArmarX package awesome_robotics"
    ```
    `--all` allows to add ignored files. 
    Only do that for specific files, such as the `build/.gitkeep`. 
    Ignored files are usually ignored for a reason.


Do not forget to push when you are done:
```shell
git push
```
