[[_TOC_]]

# How to Find the Name of an ArmarX Package

The name of an ArmarX package is the name of the project in CMake
(all ArmarX packages are CMake projects).
This might be different from the directory name. 
For example, Axii clones the ArmarX **package** (or CMake project) `armar6_skills` 
into the **directory** `armarx_integration/robots/armar6/skills` inside your workspace,
while the **package** `ArmarXSimulation` 
is currently located in the **directory** `armarx/ArmarXSimulation`.

There are different ways to find out the name of an ArmarX package.

## Open Package in QtCreator

QtCreator shows the name of an open CMake project in the Projects view (usually to the left of the editor):

![Open Scenario Dialog: Found Package](res/qtcreator-projects.png "The dialog to open a package.")


## Run CMake in Package

Run `cmake ..` in the `build` directory of the project. You should see something like this:
```
== Setting up ArmarX project ...
-- ArmarX legacy package.
-- Configuring ArmarX project `ArmarXSimulation`.
```
for the package `ArmarXSimulation` or
```
== Setting up ArmarX project ...
-- ArmarX next generation package.
-- Configuring ArmarX project `armar6_skills`.
```
for `armar6_skills` at the upper area of the output (you may need to scroll up to see it).


## Read the `CMakeLists.txt` of the Package

Look at the top-level `CMakeLists.txt` of a package. 
  For example, `ArmarXSimulation` looks like this:
```cmake
cmake_minimum_required(VERSION 3.10.2)

find_package("ArmarXCore" REQUIRED)
include(${ArmarXCore_USE_FILE})

[...]

# Name for the project
armarx_project("ArmarXSimulation")
[...]
```
The CMake project / ArmarX package name is set with the `armarx_project()` macro.
This looks simple enough (note that the quotes `""` are optional):
```
armarx_project(ArmarXCore)          =>  name = ArmarXCore
armarx_project("ArmarXSimulation")  =>  name = "ArmarXSimulation"
```

Modern ArmarX packages can have a namespace.
For instance, `armar6_skills` starts like this:

```cmake
cmake_minimum_required(VERSION 3.18)

find_package(ArmarXCore REQUIRED)
include(${ArmarXCore_USE_FILE})

[...]

# Name for the project.
armarx_enable_modern_cmake_project()
armarx_project(skills NAMESPACE armar6)
[...]
```

In that case, the ArmarX package / CMake project name
is `namespace_project`, i.e. the namespace is added in front of the project name with a `_`.
For example:
```
armarx_project(control NAMESPACE armarx)        =>  name = "armarx_control"
armarx_project(navigation NAMESPACE armarx)     =>  name = "armarx_navigation"
armarx_project(skills NAMESPACE armar6)         =>  name = "armar6_skills"
```
