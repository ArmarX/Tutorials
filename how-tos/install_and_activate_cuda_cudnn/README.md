[[_TOC_]]


# How to Install and Activate CUDA / cuDNN

## Install CUDA / cuDNN with Axii

In Axii, there are modules for CUDA/cuDNN under [`deps/cuda/`](https://gitlab.com/ArmarX/meta/setup/-/tree/main/data/modules/deps/cuda). 
For example:

```shell
$ axii list --all | grep -i cuda
│   ├── cuda/
│   │   ├── cuda-10.1-cudnn-7.6: deps/cuda/cuda-10.1-cudnn-7.6
│   │   └── cuda-11.2-cudnn-8.1: deps/cuda/cuda-11.2-cudnn-8.1
```

Each module corresponds to the combination of a specific CUDA version and a specific cuDNN version.

**Note**: If you need another version, please just request it and it will be added accordingly.

You can add such a module just as any Axii module.
For example, for CUDA 10.1 / cuDNN 7.6:

```shell
axii add deps/cuda/cuda-10.1-cudnn-7.6
```

Afterwards, an `axii upgrade` will install the requested CUDA/cuDNN.
For this to work though, Axii relies on the configuration variable variable `cuda_cudnn_archives_path` 
to point to a directory containing the downloaded archives and run scripts.

You should be prompted for a corresponding configuration by the CUDA module. On a lab PC, for example, the following will configure Axii properly: `axii config global cuda_cudnn_archives_path /common/share/archive/software/linux/cuda_cudnn`. Otherwise you can mount the H2T archive and supply the corresponding path `<archive>/software/linux/cuda_cudnn`.

You can also download the archives yourself from the NVIDIA website.
The directory pointed to by `axii config global cuda_cudnn_archives_path` is expected to look like this:
```shell 
$ ls $(axii config global cuda_cudnn_archives_path)
cuda_10.0.130_410.48_linux.run
cuda_10.1.105_418.39_linux.run
cuda_11.2.2_460.32.03_linux.run
cudnn-10.0-linux-x64-v7.4.1.5.tgz
cudnn-10.1-linux-x64-v7.6.5.32.tgz
cudnn-10.1-linux-x64-v8.0.4.30.tgz
cudnn-11.2-linux-x64-v8.1.1.33.tgz
```

## Activate CUDA / cuDNN

Axii does **not** activate any installed CUDA/cuDNN version automatically
(e.g. by exporting `CUDA_HOME`). 
This is required to allow you to use multiple versions in your workspace (e.g. for different projects).

Instead, Axii exports functions for activating and deactivating a specific CUDA/cuDNN combination
into your workspace. For example:
```shell
activate_cuda_10_1_cudnn_7_6     # Enables CUDA 10.1 / cuDNN 7.6
deactivate_cuda_10_1_cudnn_7_6   # Disables CUDA 10.1 / cuDNN 7.6.
```

> Only use a `deactivate_cuda_*` function after calling the corresponding `activate_cuda_*` function.
>
These functions export a number of environment variables, including
`CUDA_HOME`, `CUDA_TOOLKIT_ROOT_DIR`, `PATH` and `LD_LIBRARY_PATH`.

> The scripts defining these functions are located in Axii at 
> [deps/cuda/scripts/activate_cuda_*](https://gitlab.com/ArmarX/meta/setup/-/tree/main/data/modules/deps/cuda/scripts).

If you need to activate a CUDA/cuDNN to run a specific executable,
you can create a proxy script which activates it and calls the executable.
For example:

```bash
#! /bin/bash

activate_cuda_10_1_cudnn_7_6

./my_executable $@

deactivate_cuda_10_1_cudnn_7_6
```
