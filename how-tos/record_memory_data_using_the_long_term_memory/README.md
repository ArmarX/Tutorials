[[_TOC_]]


# How to Record Memory Data using the Long-Term Memory (LTM)

When using the ArmarX memory system, you can start and stop memory recordings on the fly
and write the results to disk. 
For that, you can use the _Memory Viewer_ widget, which will store recorded data in `/tmp/MemoryExport`. 
However, there are some configuration options you might need to consider.

> **Note:** 
> The method presented here will write the memory data as ARON-files. 
> These files are most suitable for memory recording and replay, but less suited for external analysis.


## Configuration

Currently, only memories that are removed from the working memory will be written into long term memory during recording. 
Thus, setting the working memory sizes of the memories of interest to small values 
will cause the memories to be immediately discarded and moved into the long term memory.


## Recording

- Inside the ArmarX GUI, open the Memory Viewer widget.
- Navigate to the first pane, **Query Settings**.
- Select all memories you want to record using the checkboxes.
- Press **Start LTM Recording** / **Stop LTM Recording** to start / stop the recording.


## Query and Store

The **Query and Store in LTM** button writes all contents of the current working memory to disk.
