[[_TOC_]]


# How to Record Multiple Image Streams Simultaneously

* The component `ImageRecordingManager` and the associated GUI plugin `ImageRecorder` allow **distributed, synchronous recording of multiple image providers**.
* Each provider writes its own data (locally) - the `ImageRecordingManager` only orchestrates (i.e. configures, starts, stops) the recordings.


## Steps

1. Start component `ImageRecordingManager` (for a quick recording, you can use the scenario `ImageRecordingScenario` from VisionX)
2. Open GUI plugin `ImageRecorder` (connects to the `ImageRecordingManager`)
3. Define the recording path (where to store data) as an ArmarX data path in the GUI (e.g., `ArmarXCore` + `recordings` maps to `.../ArmarXCore/data/ArmarXCore/recordings/`)
4. Start the image providers
5. Configure the image providers (e.g. storage format)
5. Start the recording
6. ...
7. Stop the recording


## Storing the Configuration

If you want to save the configuration for your specific application:
* Create your own scenario, add an `ImageRecordingManager`
* Do the steps above to configure the image providers
* Copy the configuration JSON (string) from the GUI
* Paste the configuration JSON (string) to the property `config_json` of the `ImageRecordingManager`

The recording configuration is now part of the scenario configuration and will be loaded by the `ImageRecordingManager` on start up.

## Viewing the Recordings
A method for viewing the recordings in the ArmarX Gui is described [here](../view_images_using_playback_image_provider).
