[[_TOC_]]

# How to Set Up QtCreator for ArmarX

> **Note:** 
> Using **flatpak** to install QtCreator is **not** recommended.
> The flatpak installation of QtCreator may not optimally use / interact 
> with the system environment. 
> If in doubt, install via the official installer (see instructions below).


## Check the CMake Version
 
Current versions of QtCreator require a newer version of CMake (3.21) 
than the Ubuntu 18.04 packages version (3.10).
[Axii](https://gitlab.com/ArmarX/meta/setup) 
automatically sets up the required CMake version in your workspace.
You can find it in `$ARMARX_WORKSPACE/tools/cmake`.

You can check the used CMake executable and version like this: 
```shell
$ which cmake
/.../code/tools/cmake/3.21/cmake-3.21.3-linux-x86_64/bin/cmake

$ cmake --version 
cmake version 3.21.2

CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

This is the version that should be used with QtCreator.


## Install QtCreator 

### Using Axii

Axii has modules for different versions of QtCreator.
To install QtCreator 5.0 (recommended on Ubuntu 18), 
add the respective module and run an upgrade:

```shell
axii workspace add tools/qtcreator/5.0
axii workspace upgrade
```


### Using the Official Installer

* Download the Qt Online Installer from [here](https://download.qt.io/official_releases/qtcreator/5.0/5.0.3/qt-creator-opensource-linux-x86_64-5.0.3.run). The downloaded file should be called `qt-creator-opensource-linux-x86_64-5.0.3.run`.
* Move the installer to a suitable directory, e.g. `~/opt/qtcreator/`

```shell
$ mkdir -p ~/opt/qtcreator
$ cd ~/opt/qtcreator
$ mv ~/Downloads/qt-creator-opensource-linux-x86_64-5.0.3.run .
```

* Execute the installer:

```shell
$ chmod u+x qt-creator-opensource-linux-x86_64-5.0.3.run
$ ./qt-creator-opensource-linux-x86_64-5.0.3.run
```

- In the installer, you first have to enter some account credentials. If you don't want to create an account, ask your supervisor. Then, press _Next_.
- Read the license obligations, then check the two boxes "I have read and approve the obligations of using Open Source Qt" and "I am an individual person not using Qt for any company. Then, press _Next_.
- In the next screen, you will be told that there is no commercial license available in your Qt account. This is okay, you can continue with the Qt Open-Source installation. Press _Next_.
- Specify the installation directory, e.g. `~/opt/qtcreator/qtcreator`, then press _Next_.
- Next, you can choose which components to install. There should only be the "Developer and Designer Tools", which are already selected. Press _Next_.
- Again, read and accept the agreement, then press _Next_.
- Finally, press _Install_.
- Wait until installation is complete.

To map the command `qtcreator` to the new version, add this line to your `~/.bashrc`:

```shell
export PATH="$HOME/opt/qtcreator/qtcreator/bin:$PATH"
```
Source your `~/.bashrc` (`source ~/.bashrc`) or open a new terminal, then confirm that the new version is used:

```shell
$ which qtcreator
/.../opt/qtcreator/qtcreator/bin/qtcreator
$ qtcreator -version

Qt Creator 5.0.3 based on Qt 5.15.2

[...]
```

Start QtCreator
```shell
qtcreator
```
and proceed by configuring like explained below.


## Configure QtCreator

You should edit some settings so they fit to our working setup and conventions. 

Open the Settings via **Tools** > **Options ...** .

* In the **Kits** section:
  * In the **CMake** tab:
    * **Clone** the _System CMake at ..._
    * Select the new entry, rename it to "ArmarX CMake", and press **Make Default**
    * Uncheck **Autorun CMake**
    * Set the path to the correct CMake installation
      * If you use the [ArmarX Setup Tool](https://gitlab.com/ArmarX/meta/setup), set the path to `$ARMARX_WORKSPACE/deps/cmake-3.21/cmake-3.21.3-linux-x86_64/bin/cmake` and help file to `$ARMARX_WORKSPACE/deps/cmake-3.21/cmake-3.21.3-linux-x86_64/doc/cmake/CMake.qch`.
      * If you installed QtCreator via **Flatpak**, prepend the path by `/run/host/` (e.g. `/run/host/usr/bin/cmake`)
      * If you [installed an up-to-date CMake without sudo](Update-CMake-(non-sudo)), set the path to `.../cmake/bin/cmake` and help file to `.../cmake/doc/cmake/CMake.qch`.
  * In the **Kits** tab:
    * **Clone** the Qt5 Kit
    * Select the new entry, rename it to "ArmarX" and press **Make Default**
    * Scroll down and set the **CMake Tool** to the newly created "ArmarX CMake"
    * Make sure a **CMake Generator** is configured. If not, press **Change ...**, select "Unix Makefiles" or "Ninja" as Generator, and press OK.
* In the **Text editor** section:
  * In the **Behavior** tab:
    * Under _Tabs And Indentation_, set select **Spaces Only** and set **Tab Size** to 4
    * Under _Mouse and Keyboard_, uncheck **Enable mouse navigation** (having it enabled might interfere with Ctrl+C/Ctrl+V)
  * In the **Display** tab:
    * Check **Display right margin at column** and set the margin to **100**
* In the **C++** section:
  * In the **File Naming** tab:
    * Check **Use "pragma once" instead of "#ifndef" guards**
    * Uncheck **Lower case file names**
* In the **Build & Run** section:
  * In the **General** tab:
    * Check **Save all files before build**
  * In the **Default Build Properties** tab:
    * Set the **Default build directory** to `build`


## Optional: Enable Autoformatting via Clang Format


### Install clang-13

Check whether `clang-13` is installed:
```shell
dpkg -V clang-13
```
If this prints nothing, `clang-13` is installed.
Otherwise, it will say `dpkg: package 'clang-13' is not installed`.

If `clang-13` is **not** installed, install it by running (requires sudo):

```shell
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
echo 'deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-13 main' | sudo tee /etc/apt/sources.list.d/llvm.list >/dev/null
sudo apt update
sudo apt install -y clang-13 clang-format-13 clang-tidy-13 lldb-13 lld-13 clangd-13
```


### Add the Beautifier Plugin to QtCreator

Enable the **_Beautifier_** Plugin in QtCreator:

* In QtCreator, go to the Plugins menu via **Help** > **About Plugins...** .
* Find **Beautifier** (under _C++_) and enable the plugin by ticking the check box.
* Restart QtCreator.

In the settings of QtCreator (**Tools** > **Options...**), find the **Beautifier** section. There:

* In the **Clang Format** tab, under _Options_:
  * If the **Clang Format command** is red, you might need to change it to **clang-format-13** or supply a full path to the executable
  * Set **Use predefined style** to "File"
  * Set **Fallback style** to "None"
* In the **General** tab:
  * (Optional) Check **Enable auto format on file save** and set **Tool** to "ClangFormat"
* (Optional) Add a key shortcut to manually trigger the formatting when you see fit:
  * In the **Environment** section, go to the **Keyboard** tab.
  * Find and select the command "FormatFile" (without a space) under "ClangFormat".
  * Add a keybinding such as `Alt+I`.
  * You can also bind other commands, such as "DisableFormattingSelectedText", e.g. to `Alt+Shift+I`

### H2T-suggested Code Style Using Axii

[Axii](https://gitlab.com/ArmarX/meta/setup) 
contains different clang format config files in the `h2t/code_style` module. You can add it to your workspace using `axii add h2t/code_style`.

### Run Clang Format Manually from the Command Line

To manually clang-format all C++ files in a package or directory, 
you can do something like this:

```shell
for file in source/**/*.{h,cpp}; do 
   clang-format-13 $file -i
; done
```
