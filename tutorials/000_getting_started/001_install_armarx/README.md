**Objective:** Learn how to use Axii to set up an ArmarX workspace and install ArmarX.

**Previous Tutorials:** None

**Next Tutorials:** [Explore ArmarX (I): Start the ARMAR-III Simulation](../002_explore_armarx_start_simulation)

**Table of Contents:**

[[_TOC_]]


# Install ArmarX

ArmarX is installed via [Axii, the ArmarX Integrated Installer](https://gitlab.com/ArmarX/meta/setup).
Follow these instructions on the linked page:
- Installation (installs Axii itself)
- Getting Started (creates an ArmarX workspace and installs the default setup of ArmarX)

You should now have an ArmarX workspace (the default is `~/code/`),
which should look roughly like this:
```
code/                    # Your ArmarX workspace.
  armarx-workspace.json  # Your workspace configuration file.
  amrarx-workspace.rc    # Your workspace environment file (generated).
  
  armarx/                # Directory containing ArmarX framework packages. 
    ArmarXCore/          # The ArmarX core package.
    ArmarXGui/           # Another ArmarX package.
    ...                  # More ArmarX packages.
  simox/                 # A standalone library for robot models, grasp planning and more.
  ...                    # More projects or directories.
```


# Add Modules

If you are a member of H2T (student or staff), consider adding these Axii modules:

- `h2t/PriorKnowledgeData`: An ArmarX package mainly containing data. It has many models of objects, scenes, and more.
- `armarx_integration/robots/armar6`: Model, simulation, skills and more related to ARMAR-6.

Please note that the module `h2t/PriorKnowledgeData` is necessary to run the following tutorials.

You can add a module by running
```shell
axii add module-name
```
for each module to add. 

Afterwards, run an upgrade:
```shell
axii upgrade
```

When finished, the new modules should be located in your workspace 
as defined by their name, e.g. at `~/code/h2t/PriorKnowledgeData`.


# Next Up

When the installation is finished, continue with [exploring ArmarX, part one](../002_explore_armarx_start_simulation).
