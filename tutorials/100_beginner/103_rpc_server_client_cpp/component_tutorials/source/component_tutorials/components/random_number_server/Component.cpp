/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    component_tutorials::ArmarXObjects::random_number_server
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/core/time/Clock.h>

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>


namespace component_tutorials::components::random_number_server
{

    const std::string
    Component::defaultName = "random_number_server";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());
        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        std::random_device randomDevice;
        this->engine = std::default_random_engine { randomDevice() };


        if (false)  // Disabled for integration test.
        {
            // Test the RNG implementation.
            std::stringstream ss;
            for (int i = 0; i < 10; ++i)
            {
                ss << "[" << (i + 1) << "] Randomly generated number: " << generateRandomNumber() << "\n";
            }
            ARMARX_IMPORTANT << ss.str();
        }
    }


    void
    Component::onConnectComponent()
    {
        // Do things after connecting to topics and components.
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }


    Ice::Int Component::generateRandomNumber(const Ice::Current&)
    {
        ARMARX_INFO << "[Server] Received request.";

        // Simulate a longer operation.
        armarx::Clock::WaitFor(armarx::Duration::Seconds(1));

        std::uniform_int_distribution<int> distribution(1, 10);
        int randomNumber = distribution(engine);

        ARMARX_INFO << "[Server] Send response: " << randomNumber;
        return randomNumber;
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

}  // namespace component_tutorials::components::random_number_server
