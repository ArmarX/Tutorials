/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    memory_tutorial::ArmarXObjects::object_memory_client
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <RobotAPI/libraries/aron/common/aron_conversions/core.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/query.h>

#include <memory_tutorial/object_instance/aron_conversions.h>
#include <memory_tutorial/object_instance/ObjectInstance.h>
#include <memory_tutorial/object_instance/aron/ObjectInstance.aron.generated.h>


namespace memory_tutorial::components::object_memory_client
{

    const std::string
    Component::defaultName = "object_memory_client";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.
        def->optional(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");
        def->optional(properties.numBoxes, "p.box.Number", "Number of boxes to draw in ArViz.");

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void
    Component::onConnectComponent()
    {
        producerConsumer.arviz = arviz;

        this->task = new armarx::SimpleRunningTask<>([this]()
        {
           this->run();
        });
        this->task->start();
    }


    void
    Component::onDisconnectComponent()
    {
        const bool join = true;
        this->task->stop(join);
        this->task = nullptr;
    }


    void
    Component::onExitComponent()
    {
    }


    void
    Component::run()
    {
        namespace armem = armarx::armem;

        const armem::MemoryID coreSegmentID("Object", "Instance");

        // Subscribe to memory updates.
        {
            armem::client::Reader reader = memoryNameSystem().getReader(coreSegmentID);
            auto callback = [this, reader](const std::vector<armem::MemoryID>& updatedSnapshotIDs)
            {
                ARMARX_IMPORTANT << "Updated IDs: " << updatedSnapshotIDs;

                armem::client::QueryResult result = reader.queryMemoryIDs(updatedSnapshotIDs);
                if (result.success)
                {
                    result.memory.forEachInstance([this](const armem::wm::EntityInstance& instance)
                    {
                        object_instance::ObjectInstance objectInstance =
                                armarx::aron::fromAron<object_instance::ObjectInstance>(
                                    object_instance::arondto::ObjectInstance::FromAron(instance.data()));

                        {
                            std::scoped_lock lock(producerConsumerMutex);
                            this->producerConsumer.consume(objectInstance);
                        }
                    });
                }
            };
            memoryNameSystem().subscribe(coreSegmentID, callback);
        }

        // Write data to the memory (commit).
        {
            // 1. Create a writer to the memory segment.
            armem::client::Writer writer = memoryNameSystem().getWriter(coreSegmentID);

            // 2. Obtain new data as business object (BO).
            object_instance::ObjectInstance bo = producerConsumer.produce();

            // 3. Convert the data to a data transfer object (DTO).
            object_instance::arondto::ObjectInstance dto;
            toAron(dto, bo);

            // 4. Build an `EntityUpdate` with this data.
            armem::EntityUpdate update;
            update.entityID = coreSegmentID
                    .withProviderSegmentName(this->getName())
                    .withEntityName(bo.name);
            update.timeCreated = armem::Time::Now();
            update.instancesData = { dto.toAron() };

            // 5. Send the `EntityUpdate` in a commit to the memory, using the `Writer` ...
            const bool singleUpdate = true;
            if (singleUpdate)
            {
                // ... by sending the update itself (if there is only one update) ...
                writer.commit(update);
            }
            else
            {
                // ... or bundle it as a commit first to commit multiple updates.
                armem::Commit commit;
                commit.add(update);
                writer.commit(commit);
            }
        }

        // Read data from the memory (query).
        {
            // 1. Create reader to the memory segment.
            armem::client::Reader reader = memoryNameSystem().getReader(coreSegmentID);

            // 2. Build a query.
            armem::client::QueryBuilder builder;

            const bool withQueryFns = true;
            if (withQueryFns)
            {
                namespace qfs = armem::client::query_fns;
                builder.coreSegments(qfs::withName(coreSegmentID.coreSegmentName))
                        .providerSegments(qfs::all())
                        .entities(qfs::withNamesContaining("amicelli"))
                        .snapshots(qfs::latest())
                        ;
            }
            else
            {
                builder.coreSegments().withName(coreSegmentID.coreSegmentName)
                        .providerSegments().all()
                        .entities().withNamesContaining("amicelli")
                        .snapshots().latest()
                        ;
            }

            // 3. Perform the query.
            armem::client::QueryResult result = reader.query(builder.buildQueryInput());

            // 4. Process the query result.
            if (result.success)
            {
                armem::wm::Memory& memory = result.memory;

                memory.forEachInstance([this](const armem::wm::EntityInstance& instance)
                {
                    object_instance::ObjectInstance objectInstance =
                            armarx::aron::fromAron<object_instance::ObjectInstance>(
                                object_instance::arondto::ObjectInstance::FromAron(instance.data()));

                    {
                        std::scoped_lock lock(producerConsumerMutex);
                        this->producerConsumer.consume(objectInstance);
                    }
                });
            }
            else
            {
                // Handle the error.
                ARMARX_INFO << "Query failed: " << result.errorMessage;
            }
        }
    }


    std::string
    Component::getDefaultName() const
    {
        return GetDefaultName();
    }


    std::string Component::GetDefaultName()
    {
        return defaultName;
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

}  // namespace memory_tutorial::components::object_memory_client
