/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    memory_tutorial::ArmarXObjects::object
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <SimoxUtility/shapes/OrientedBox.h>

#include <ArmarXCore/core/PackagePath.h>

#include <RobotAPI/libraries/core/FramedPose.h>


namespace memory_tutorial::object_instance
{

    /**
     * @brief A pose in a semantic frame.
     */
    class FramedPose
    {
    public:

        Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
        std::string frame = armarx::GlobalFrame;
        std::string agent = "";

    };


    /**
     * @brief Business Object (BO) class of object instances.
     */
    class ObjectInstance
    {
    public:

        std::string name;
        FramedPose pose;

        armarx::PackagePath fileLocation = armarx::PackagePath("PriorKnowledgeData", "");
        simox::OrientedBoxf localOobb;

    };

}
