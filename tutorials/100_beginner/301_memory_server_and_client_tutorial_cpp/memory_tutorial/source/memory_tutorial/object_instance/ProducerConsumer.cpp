#include "ProducerConsumer.h"

#include <chrono>

#include <Eigen/Core>

#include <SimoxUtility/math/pose/pose.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <memory_tutorial/object_instance/ObjectInstance.h>


namespace memory_tutorial::object_instance
{

    object_instance::ObjectInstance ProducerConsumer::produce()
    {
        const int time_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
        float speed = 0.25;
        const float time = speed * static_cast<float>(time_ms) / 1000.f;

        object_instance::ObjectInstance instance;

        instance.name = "an amicelli object instance";

        simox::math::position(instance.pose.pose) = 1000 * Eigen::Vector3f(std::sin(time), std::cos(time), 1.f);
        instance.pose.frame = armarx::GlobalFrame;
        instance.pose.agent = "";

        instance.fileLocation = armarx::PackagePath("PriorKnowledgeData", "PriorKnowledgeData/objects/KIT/Amicelli/Amicelli.xml");

        instance.localOobb = simox::OrientedBoxf(
                    Eigen::Vector3f::Zero(), Eigen::Quaternionf::Identity(),
                    100 * Eigen::Vector3f(1, 2, 3));

        return instance;
    }


    void ProducerConsumer::consume(const object_instance::ObjectInstance& instance)
    {
        ARMARX_INFO << "Consuming instance '" << instance.name << "' "
                    << "\n- pose in '" << instance.pose.frame << "': \n" << instance.pose.pose
                       ;

        if (arviz.has_value())
        {
            armarx::viz::Layer layer = arviz->layer("consume");
            armarx::data::PackagePath path = instance.fileLocation.serialize();
            layer.add(armarx::viz::Object(instance.name)
                      .file(path.package, path.path)
                      .pose(instance.pose.pose)
                      );
            layer.add(armarx::viz::Pose(instance.name + " pose")
                      .pose(instance.pose.pose)
                      .scale(2)
                      );
            arviz->commit(layer);
        }
    }

}
