# ArmarX Tutorials

Welcome to the ArmarX tutorials!

These tutorials are currently under construction and should be considered work in progress.
However, feel free to have a look and leave feedback. 
Tutorials prefixed by "(WIP)" are work in progress. 

In the meantime, you can have a look at the 
[existing tutorials in the documentation](https://armarx.humanoids.kit.edu/ArmarXDoc-Tutorials.html)
(especially the 
[ArmarXCore](https://armarx.humanoids.kit.edu/ArmarXCore-Tutorials.html), 
[ArmarXGui](https://armarx.humanoids.kit.edu/ArmarXGui-Tutorials.html)
and [RobotAPI](https://armarx.humanoids.kit.edu/RobotAPI-Tutorials.html) tutorials).


General tips for doing the tutorials:

- Really do the things in the tutorials yourself and see what the results look like.
  Not only is this more fun, you will also get a better feeling for them and memorize them better.
- This extends to code: Try to actually type code yourself in the IDE (e.g. QtCreator) 
  instead of copy-pasting it from the tutorial. This might feel cumbersome,
  but this way, you see how the IDE behaves and supports (or does not support) you 
  when writing code, which makes you ready for your own project later on.
  Also, you memorize the code and workflow better the more often you do it, 
  so you're better going to start right away!
- Sometimes, a code box looks like this:
  ```shell
  $ echo $ARMARX_WORKSPACE
  /home/.../code/
  ```
  In this case, the `$` indicates that this line is the command you should run (excluding the `$`).
  The lines below are an example for the result of the command.
  

## Getting Started

1. [Install ArmarX](000_getting_started/001_install_armarx)
2. [Explore ArmarX (I): Start the ARMAR-III Simulation](000_getting_started/002_explore_armarx_start_simulation)
3. (WIP) [Explore ArmarX (II): Interact with the ARMAR-III Simulation](000_getting_started/003_explore_armarx_interact_with_simulation)


## Beginner Level

### Components, Ice and the Distributed System

1. [Create a "Hello World!" Component in ArmarX (C++)](100_beginner/101_hello_world)
2. [Understand Distributed Systems](100_beginner/102_distributed_systems)
3. Remote Procedure Calls (RPC): Server-Client Communication 
   * [Write a Server and Client Communicating via RPC (C++)](100_beginner/103_rpc_server_client_cpp)  
4. Topics: Publisher-Subscriber Communication
   * (WIP) [Write a Publisher and Subscriber Communicating via a Topic (C++)](100_beginner/104_topics_publisher_subscriber_cpp)


### Basic Tools

1. ArViz Tutorial
2. Remote Gui Tutorial
3. (Debug Observer Tutorial?)


### The Memory System

1. [Memory Server and Client Tutorial (C++)](100_beginner/301_memory_server_and_client_tutorial_cpp)

Planned:

2. Memory: Read + Visualize once via query
3. Memory: Read + Visualize continuously via subscriptions
4. (Memory: Read + Visualize continuously via polling?)
5. Memory: Write
6. Control / RobotUnit
7. ...


## Intermediate Level

14. Image Provider (+Image Monitor)
15. Image Processor
16. Point Cloud Provider (+Point Cloud Viewer)
17. Point Cloud Processor
18. Memory-enabled processor (read, process, write)
19. Object Pose Provider
20. Object Pose Client
21. ...


## Advanced Level

1. Adopt Armar3 Exercise from Roboterpraktikum
2. ...
