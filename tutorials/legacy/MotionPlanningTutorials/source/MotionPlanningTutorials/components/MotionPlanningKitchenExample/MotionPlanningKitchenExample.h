/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MotionPlanningTutorials
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_COMPONENT_MotionPlanningTutorials_MotionPlanningKitchenExampleComponent_H
#define _ARMARX_COMPONENT_MotionPlanningTutorials_MotionPlanningKitchenExampleComponent_H

#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>

#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>

#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>
#include <RobotComponents/components/MotionPlanning/Tasks/PathCollection/Task.h>

namespace armarx
{
    class MotionPlanningKitchenExampleComponent;

    typedef IceInternal::Handle<MotionPlanningKitchenExampleComponent> ExamplePlanningComponentPtr;

    class MotionPlanningKitchenExampleComponentPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        MotionPlanningKitchenExampleComponentPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PlanningServerName", "MotionPlanningServer", "The planning server's name.");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "The used working memory. (for objects)");
        }
    };

    /**
     * @brief This example uses RRTConnect to plan a motion for grasping and refines its exampe using the \ref armarx::RandomShortcutPostprocessor.
     * Furthermore a path is planned using A* and RRTConnect+RandomShortcutPostprocessor and the result is compared.
     * This example needs the Kitchen snapshot loaded (start the Armar3Simulator)
     */
    class MotionPlanningKitchenExampleComponent:
        virtual public Component
    {
    public:
        /**
         * @brief ctor
         */
        MotionPlanningKitchenExampleComponent() = default;
        /**
         * @brief dtor
         */
        ~MotionPlanningKitchenExampleComponent() override = default;

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "MotionPlanningKitchenExampleComponent";
        }
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override {}
        void onExitComponent() override {}

        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new MotionPlanningKitchenExampleComponentPropertyDefinitions(getConfigIdentifier()));
        }

        CSpaceBasePtr createCSpaceForGrasping(bool minimal);
        CSpaceBasePtr createCSpaceForWalking();

        void runGrasp(bool minimal);
        void runWalk();


    protected:
        std::string pServerName;
        std::string wMemName;

        memoryx::WorkingMemoryInterfacePrx wMemProxy;
        MotionPlanningServerInterfacePrx pServerProxy;

        memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx;
        memoryx::CommonStorageInterfacePrx cStorageProxy;

        PathCollectionHandle graspBowl;
        PathCollectionHandle graspBowlMin;
        PathCollectionHandle walk;
    };
}
#endif
