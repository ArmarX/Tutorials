/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MultiThreadingExample::ArmarXObjects::MultiThreadingExampleClient
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MultiThreadingExampleClient.h"


using namespace armarx;


void MultiThreadingExampleClient::onInitComponent()
{
    usingProxy(getProperty<std::string>("ServerName").getValue());
    isSender = getProperty<std::string>("ClientType").getValue() == "Sender";
}


void MultiThreadingExampleClient::onConnectComponent()
{
    serverPrx = getProxy<MultiThreadingExampleServerInterfacePrx>(getProperty<std::string>("ServerName").getValue());
    clientTask = new PeriodicTask<MultiThreadingExampleClient>(this, &MultiThreadingExampleClient::periodicTaskCallback, 700);
    clientTask->start();
}


void MultiThreadingExampleClient::onDisconnectComponent()
{
    clientTask->stop();
}


void MultiThreadingExampleClient::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr MultiThreadingExampleClient::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new MultiThreadingExampleClientPropertyDefinitions(
            getConfigIdentifier()));
}

void MultiThreadingExampleClient::periodicTaskCallback()
{
    if (isSender)
    {
        counter++;
        ARMARX_IMPORTANT << "Setting the Counter " << counter;
        serverPrx->setValue(ValueToString(counter));
    }
    else
    {
        ARMARX_IMPORTANT << "Getting the counter: " << serverPrx->getValue();
    }
}

