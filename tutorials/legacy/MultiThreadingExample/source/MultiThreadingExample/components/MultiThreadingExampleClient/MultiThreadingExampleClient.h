/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MultiThreadingExample::ArmarXObjects::MultiThreadingExampleClient
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MultiThreadingExample_MultiThreadingExampleClient_H
#define _ARMARX_COMPONENT_MultiThreadingExample_MultiThreadingExampleClient_H


#include <ArmarXCore/core/Component.h>

#include <MultiThreadingExample/interface/MultiThreadingExampleServerInterface.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

namespace armarx
{
    /**
     * @class MultiThreadingExampleClientPropertyDefinitions
     * @brief
     */
    class MultiThreadingExampleClientPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        MultiThreadingExampleClientPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("ClientType", "Sender or Receiver");
            defineOptionalProperty<std::string>("ServerName", "MultiThreadingExampleServer", "Description");
        }
    };

    /**
     * @defgroup Component-MultiThreadingExampleClient MultiThreadingExampleClient
     * @ingroup MultiThreadingExample-Components
     * A description of the component MultiThreadingExampleClient.
     *
     * @class MultiThreadingExampleClient
     * @ingroup Component-MultiThreadingExampleClient
     * @brief Brief description of class MultiThreadingExampleClient.
     *
     * Detailed description of class MultiThreadingExampleClient.
     */
    class MultiThreadingExampleClient :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "MultiThreadingExampleClient";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        bool isSender;
        int counter = 0;
        MultiThreadingExampleServerInterfacePrx serverPrx;
        PeriodicTask<MultiThreadingExampleClient>::pointer_type clientTask;

        virtual void periodicTaskCallback();
    };
}

#endif
