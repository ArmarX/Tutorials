armarx_component_set_name("MultiThreadingExampleServer")

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore MultiThreadingExampleInterfaces)

set(SOURCES MultiThreadingExampleServer.cpp)
set(HEADERS MultiThreadingExampleServer.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
