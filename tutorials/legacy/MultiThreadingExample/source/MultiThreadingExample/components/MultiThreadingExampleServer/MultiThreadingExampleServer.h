/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MultiThreadingExample::ArmarXObjects::MultiThreadingExampleServer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MultiThreadingExample_MultiThreadingExampleServer_H
#define _ARMARX_COMPONENT_MultiThreadingExample_MultiThreadingExampleServer_H


#include <ArmarXCore/core/Component.h>

#include <MultiThreadingExample/interface/MultiThreadingExampleServerInterface.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <mutex>

namespace armarx
{
    /**
     * @class MultiThreadingExampleServerPropertyDefinitions
     * @brief
     */
    class MultiThreadingExampleServerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        MultiThreadingExampleServerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-MultiThreadingExampleServer MultiThreadingExampleServer
     * @ingroup MultiThreadingExample-Components
     * A description of the component MultiThreadingExampleServer.
     *
     * @class MultiThreadingExampleServer
     * @ingroup Component-MultiThreadingExampleServer
     * @brief Brief description of class MultiThreadingExampleServer.
     *
     * Detailed description of class MultiThreadingExampleServer.
     */
    class MultiThreadingExampleServer :
        virtual public armarx::Component,
        virtual public armarx::MultiThreadingExampleServerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "MultiThreadingExampleServer";
        }

        // MultiThreadingExampleServerInterface interface
        void setValue(const std::string& value, const Ice::Current&) override;
        std::string getValue(const Ice::Current&) override;

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        virtual void runningTask();

        std::string inputValue, outputValue;
        RunningTask<MultiThreadingExampleServer>::pointer_type serverTask = new RunningTask<MultiThreadingExampleServer>(this, &MultiThreadingExampleServer::runningTask);
        std::mutex inputMutex, outputMutex;
    };
}

#endif
