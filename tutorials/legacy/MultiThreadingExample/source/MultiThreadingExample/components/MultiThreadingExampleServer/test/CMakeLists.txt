
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore MultiThreadingExampleServer)
 
armarx_add_test(MultiThreadingExampleServerTest MultiThreadingExampleServerTest.cpp "${LIBS}")