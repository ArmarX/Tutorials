###
### CMakeLists.txt file for MultiThreadingExample Interfaces
###

# Dependencies on interface libraries to other ArmarX Packages must be specified
# in the following variable separated by whitespaces
# set(MultiThreadingExample_INTERFACE_DEPEND ArmarXCore)

# List of slice files to include in the interface library
set(SLICE_FILES
    MultiThreadingExampleServerInterface.ice
)

# generate the interface library
armarx_interfaces_generate_library(MultiThreadingExample "${MultiThreadingExample_INTERFACE_DEPEND}")
