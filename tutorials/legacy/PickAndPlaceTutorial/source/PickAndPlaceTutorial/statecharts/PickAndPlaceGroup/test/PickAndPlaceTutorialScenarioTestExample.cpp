/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::Tutorials::PickAndPlaceTutorialScenarioTestExample
#define ARMARX_BOOST_TEST

#include <PickAndPlaceTutorial/Test.h>

// Includes the environment for testing whole statechart-scenarios with access to memory components
#include <MemoryX/core/test/StatechartScenarioMemoryTestEnv.h>

#include <boost/test/floating_point_comparison.hpp>

using namespace armarx;
using namespace memoryx;

BOOST_AUTO_TEST_CASE(PickAndPlaceTutorialScenarioTestExample)
{
    // Initialising the testing-environment, but this time with access to memory components.
    // It is possible to set the path to a specific database, that should be used for this test.
    StatechartScenarioMemoryTestEnvironment env("PickAndPlaceTutorialScenarioTestExample", "ArmarXDB", "ArmarXDB/dbexport/memdb");
    env.startScenario("ArmarXSimulation", "Armar3Simulation", "Simulator", 5000);
    env.startScenario("PickAndPlaceTutorial", "PickAndPlaceScenario");

    env.watcher->waitForStateChartFinished(90000);

    // The output-parameters of the statechart are not used in this example, but they get accessed like this
    StringVariantContainerBaseMap statechartOutput = env.watcher->getStatechartOutput();


    // Trying to check the position of the objectInstance "vitaliscereal" after the statechart has finished correctly
    try
    {
        ObjectInstanceMemorySegmentBasePrx objectInstances = env.memoryAccess->workingMemory->getObjectInstancesSegment();
        ObjectInstancePtr vitaliscereal = ObjectInstancePtr::dynamicCast(objectInstances->getObjectInstanceByName("vitaliscereal"));

        BOOST_CHECK_EQUAL(vitaliscereal, !false);

        float eps = 200.0f; // absolute

        int expectedXPosition = 2089;
        int expectedYPosition = 5221;
        int expectedZPosition = 788;

        BOOST_CHECK_SMALL(vitaliscereal->getPosition()->x - expectedXPosition, eps);
        BOOST_CHECK_SMALL(vitaliscereal->getPosition()->y - expectedYPosition, eps);
        BOOST_CHECK_SMALL(vitaliscereal->getPosition()->z - expectedZPosition, eps);

    }
    catch (...)
    {
        handleExceptions();
    }
}
