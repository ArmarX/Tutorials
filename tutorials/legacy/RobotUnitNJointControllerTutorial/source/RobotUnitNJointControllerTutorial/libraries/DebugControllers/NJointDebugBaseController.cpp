/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointDebugBaseController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NJointDebugBaseController.h"

using namespace armarx;


WidgetDescription::WidgetPtr NJointDebugBaseController::GenerateConfigDescription(const VirtualRobot::RobotPtr&, const std::map<std::string, ConstControlDevicePtr>&, const std::map<std::string, ConstSensorDevicePtr>&)
{
    return new WidgetDescription::HBoxLayout;
}

NJointDebugBaseController::ConfigPtrT NJointDebugBaseController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
{
    return new NJointControllerConfig{};
}

void NJointDebugBaseController::callDescribedFunction(const std::string&, const StringVariantBaseMap&, const Ice::Current&)
{
    triggered = true;
}

WidgetDescription::StringWidgetDictionary NJointDebugBaseController::getFunctionDescriptions(const Ice::Current&) const
{
    return {{"trigger", nullptr}};
}

NJointDebugBaseController::NJointDebugBaseController(const RobotUnitPtr& ru, NJointDebugBaseController::ConfigPtrT, const VirtualRobot::RobotPtr& r)
{
    bool skip = false;
    for (const auto& name : r->getRobotNodeNames())
    {
        const auto cdev = peekControlDevice(name);
        if (!cdev || !cdev->getJointController(ControlModes::Velocity1DoF))
        {
            continue;
        }

        if (skip)
        {
            skip = false;
        }
        else
        {
            skip = true;
            ControlTargetBase* ct = useControlTarget(name, ControlModes::Velocity1DoF);
            ARMARX_CHECK_EXPRESSION(ct->isA<ControlTarget1DoFActuatorVelocity>());
            targets.emplace_back(ct->asA<ControlTarget1DoFActuatorVelocity>());
        }
    }
    ARMARX_CHECK_EXPRESSION(!targets.empty());
}

void NJointDebugBaseController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
{
    for (auto t : targets)
    {
        t->velocity = 0;
    }
}

void NJointDebugBaseController::rtPreActivateController()
{
    triggered = false;
}
