/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointDebugBaseController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(NJointDebugBaseController);

    /**
    * @ingroup RobotUnitNJointControllerTutorial
    * @brief Convinience base for debugging controllers
    */
    class NJointDebugBaseController: public NJointController
    {
    public:
        static WidgetDescription::WidgetPtr GenerateConfigDescription
        (
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&
        );
        static ConfigPtrT GenerateConfigFromVariants(const StringVariantBaseMap& values);
        void callDescribedFunction(const std::string&, const StringVariantBaseMap&, const Ice::Current&) override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;


        NJointDebugBaseController(const RobotUnitPtr& ru, ConfigPtrT, const VirtualRobot::RobotPtr&);

        void rtRun(const IceUtil::Time& = {}, const IceUtil::Time& = {}) override;
    protected:
        void rtPreActivateController() override;


        std::atomic<bool> triggered{false};
        std::vector<ControlTarget1DoFActuatorVelocity*> targets;
    };
}
