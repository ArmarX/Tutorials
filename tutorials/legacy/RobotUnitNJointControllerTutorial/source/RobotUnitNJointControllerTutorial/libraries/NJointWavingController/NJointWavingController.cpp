/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointWavingController
 * @author     Raphael ( ufdrv at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <boost/algorithm/clamp.hpp>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <ArmarXGui/libraries/DefaultWidgetDescriptions/DefaultWidgetDescriptions.h>

#include <RobotAPI/libraries/core/Pose.h>

#include "NJointWavingController.h"

using namespace armarx;

WidgetDescription::WidgetPtr NJointWavingController::GenerateConfigDescription(const VirtualRobot::RobotPtr&, const std::map<std::string, ConstControlDevicePtr>&, const std::map<std::string, ConstSensorDevicePtr>&)
{
    using namespace armarx::WidgetDescription;
    return makeHBoxLayout(
    {
        makeLabel("Use"),
        makeCheckBox("LeftArm", true, "left arm"),
        makeCheckBox("RightArm", false, "right arm")
    });
}

NJointWavingControllerConfigPtr NJointWavingController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
{
    return new NJointWavingControllerConfig {values.at("LeftArm")->getBool(), values.at("RightArm")->getBool()};
}

NJointWavingController::NJointWavingController(
    RobotUnitPtr prov,
    NJointWavingControllerConfigPtr cfg,
    const VirtualRobot::RobotPtr& r)
{
    static const std::set<std::string> supportedRobots {"Armar3", "Armar4", "Armar6"};
    ARMARX_CHECK_EXPRESSION(supportedRobots.count(r->getName()));
    ARMARX_CHECK_EXPRESSION_W_HINT(cfg->leftArm || cfg->rightArm, "At least one arm has to be used");

    const auto addArmar3Arm = [&](const std::string & LR)
    {
        ctrls.emplace_back();
        JointsCtrl& ctrl = ctrls.back();
        ctrl.joints.reserve(5);
        ctrl.joints.emplace_back(this, "Shoulder 1 " + LR, +0, +0, +0);
        ctrl.joints.emplace_back(this, "Shoulder 2 " + LR, +1.3090, +1.3090, +1.3090);
        ctrl.joints.emplace_back(this, "Upperarm "   + LR, -1.2217, -1.2217, -1.2217);
        ctrl.joints.emplace_back(this, "Elbow "      + LR, +0, -0.2617, +0.2617);
        ctrl.joints.emplace_back(this, "Underarm "   + LR, +M_PI / 2, +M_PI / 2, +M_PI / 2);
    };
    const auto addArmar4Arm = [&](const std::string & LR)
    {
        ctrls.emplace_back();
        JointsCtrl& ctrl = ctrls.back();
        ctrl.joints.reserve(5);
        ctrl.joints.emplace_back(this, "Arm" + LR + "_Sho1_joint", +0.01, +0.01, +0.01);
        ctrl.joints.emplace_back(this, "Arm" + LR + "_Sho2_joint", +0, +0, +0);
        ctrl.joints.emplace_back(this, "Arm" + LR + "_Sho3_joint", +1.3, +1.3, +1.3);
        ctrl.joints.emplace_back(this, "Arm" + LR + "_Sho4_joint", +1.3000, +1.3000, +1.3000);
        ctrl.joints.emplace_back(this, "Arm" + LR + "_Elb1_joint", +0.5000, +0.5000, +1.5000);
    };
    const auto addArmar6Arm = [&](const std::string & LR)
    {
        ctrls.emplace_back();
        JointsCtrl& ctrl = ctrls.back();
        const float direction = "R" == LR ? 1 : -1;
        const float sho3 = direction * +M_PI / 2;
        const float elb2 = direction * -M_PI / 2;
        ctrl.joints.reserve(5);
        ctrl.joints.emplace_back(this, "Arm" + LR + "2_Sho1", +0, +0, +0);
        ctrl.joints.emplace_back(this, "Arm" + LR + "3_Sho2", +M_PI / 2, +M_PI / 2, +M_PI / 2);
        ctrl.joints.emplace_back(this, "Arm" + LR + "4_Sho3", sho3, sho3, sho3);
        ctrl.joints.emplace_back(this, "Arm" + LR + "5_Elb1", +0, +1.3090, +1.8325);
        ctrl.joints.emplace_back(this, "Arm" + LR + "6_Elb2", elb2, elb2, elb2);
    };
    if (r->getName() == "Armar3")
    {
        if (cfg->leftArm)
        {
            addArmar3Arm("L");
        }
        if (cfg->rightArm)
        {
            addArmar3Arm("R");
        }
    }
    if (r->getName() == "Armar4")
    {
        if (cfg->leftArm)
        {
            addArmar4Arm("L");
        }
        if (cfg->rightArm)
        {
            addArmar4Arm("R");
        }
        maxV = 0.3;
        p = 0.0075;
    }
    if (r->getName() == "Armar6")
    {
        if (cfg->leftArm)
        {
            addArmar6Arm("L");
        }
        if (cfg->rightArm)
        {
            addArmar6Arm("R");
        }
    }
}

void NJointWavingController::rtRun(const IceUtil::Time&, const IceUtil::Time& timeSinceLastIteration)
{
    const auto dt = static_cast<float>(timeSinceLastIteration.toMicroSecondsDouble());
    for (auto& ctrl : ctrls)
    {
        ctrl.run(dt, maxV, p);
    }
}

void NJointWavingController::JointsCtrl::run(float dt, float maxV, float p)
{
    float maxError = 0;
    for (auto& joint : joints)
    {
        maxError = std::max(maxError, std::abs(joint.run(dt, waypoint, maxV, p)));
    }
    if (maxError < 0.1)
    {
        // 0 -> 1
        // 1 -> 2
        // 2 -> 1
        waypoint = 1 + (waypoint % 2);
    }
}

NJointWavingController::JointsCtrl::JointData::JointData(NJointController* ctrl, const std::string& name, float pre, float wp1, float wp2):
    waypoints {pre, wp1, wp2},
    cpos {ctrl->useSensorValue<const SensorValue1DoFActuatorPosition>(name)},
    tvel {ctrl->useControlTarget<ControlTarget1DoFActuatorVelocity>(name, ControlModes::Velocity1DoF)}
{
    ARMARX_CHECK_EXPRESSION_W_HINT(cpos, "no pos sensor for " << name << " of type " << ctrl->peekSensorDevice(name)->getSensorValueType());
    ARMARX_CHECK_EXPRESSION_W_HINT(tvel, "no vel control for " << name << "! modes =\n" << ctrl->peekControlDevice(name)->getControlModes());
}

float NJointWavingController::JointsCtrl::JointData::run(float dt, std::size_t waypoint, float maxV, float p)
{
    dt = std::min(dt, p);
    const float delta = waypoints.at(waypoint) - cpos->position;
    tvel->velocity = boost::algorithm::clamp(0.01 * delta / dt, -maxV, maxV);
    return std::abs(delta);
}

NJointControllerRegistration<NJointWavingController> registrationControllerLvl1WavingController("NJointWavingController");
ARMARX_ASSERT_NJOINTCONTROLLER_HAS_CONSTRUCTION_GUI(NJointWavingController);
