/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointWavingController
 * @author     Raphael ( ufdrv at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_LIB_RobotUnitNJointControllerTutorial_NJointWavingController_H
#define _ARMARX_LIB_RobotUnitNJointControllerTutorial_NJointWavingController_H

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(NJointWavingController);
    TYPEDEF_PTRS_HANDLE(NJointWavingControllerConfig);

    class NJointWavingControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointWavingControllerConfig(bool leftArm, bool rightArm):
            leftArm {leftArm},
            rightArm {rightArm}
        {}
        const bool leftArm;
        const bool rightArm;
    };
    /**
    * @defgroup Library-NJointWavingController NJointWavingController
    * @ingroup RobotUnitNJointControllerTutorial
    * A description of the library NJointWavingController.
    *
    * @class NJointWavingController
    * @ingroup Library-NJointWavingController
    * @brief Brief description of class NJointWavingController.
    *
    * Detailed description of class NJointWavingController.
    */
    class NJointWavingController: public NJointController
    {
    public:
        using ConfigPtrT = NJointWavingControllerConfigPtr;

        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointWavingControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointWavingController(
            RobotUnitPtr prov,
            NJointWavingControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);

        std::string getClassName(const Ice::Current&) const override
        {
            return "NJointWavingController";
        }
        void rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& timeSinceLastIteration) override;

        struct JointsCtrl
        {
            struct JointData
            {
                JointData(NJointController* ctrl, const std::string& name, float pre, float wp1, float wp2);
                const std::array<float, 3> waypoints;
                const SensorValue1DoFActuatorPosition* const cpos;
                ControlTarget1DoFActuatorVelocity* const tvel;
                float run(float dt, std::size_t waypoint, float maxV, float p);
            };
            std::vector<JointData> joints;
            std::size_t waypoint {0};
            void run(float dt, float maxV, float p);
        };
        std::vector<JointsCtrl> ctrls;

        float maxV {0.5};
        float p {0.01};
    };
}
#endif
