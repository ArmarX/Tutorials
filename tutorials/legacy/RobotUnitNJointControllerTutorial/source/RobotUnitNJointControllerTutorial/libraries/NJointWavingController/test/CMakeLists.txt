
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore NJointWavingController)
 
armarx_add_test(NJointWavingControllerTest NJointWavingControllerTest.cpp "${LIBS}")
