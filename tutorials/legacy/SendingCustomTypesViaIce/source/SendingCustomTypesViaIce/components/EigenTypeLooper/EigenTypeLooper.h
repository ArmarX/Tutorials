/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SendingCustomTypesViaIce::ArmarXObjects::EigenTypeLooper
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <SendingCustomTypesViaIce/interface/EigenTypeLooper.h>


namespace armarx
{
    /**
     * @defgroup Component-EigenTypeLooper EigenTypeLooper
     * @ingroup SendingCustomTypesViaIce-Components
     * A description of the component EigenTypeLooper.
     *
     * @class EigenTypeLooper
     * @ingroup Component-EigenTypeLooper
     * @brief Brief description of class EigenTypeLooper.
     *
     * Detailed description of class EigenTypeLooper.
     */
    class EigenTypeLooper :
        virtual public armarx::Component,
        virtual public EigenTypeLooperInterface
    {
    public:
        std::string getDefaultName() const override;

        EigenTypeLooperInterfacePrx topic;
        void v3(const Eigen::Vector3f& v3, const Ice::Current&) override;
        void q(const Eigen::Quaterniond& q, const Ice::Current&) override;
        void vx(const Eigen::VectorXf& vx, const Ice::Current&) override;
        void rvx(const Eigen::RowVectorXf& vx, const Ice::Current&) override;
        void mX(const Eigen::MatrixXf& vx, const Ice::Current&) override;
        void m2X(const Eigen::Matrix2Xf& vx, const Ice::Current&) override;
        void mX2(const Eigen::MatrixX2f& vx, const Ice::Current&) override;

    protected:
        void onInitComponent() override;
        void onConnectComponent() override;

        void onDisconnectComponent() override {}
        void onExitComponent() override {}

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    };
}
