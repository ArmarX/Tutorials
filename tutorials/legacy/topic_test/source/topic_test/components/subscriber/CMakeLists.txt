armarx_add_component(subscriber
    ICE_FILES
        ComponentInterface.ice
    ICE_DEPENDENCIES
        ArmarXCoreInterfaces
        # RobotAPIInterfaces
        core_interfaces
    # ARON_FILES
        # aron/my_type.xml
    SOURCES
        Component.cpp
    HEADERS
        Component.h
    DEPENDENCIES
        # ArmarXCore
        ArmarXCore
        ## ArmarXCoreComponentPlugins  # For DebugObserver plugin.
        # ArmarXGui
        ## ArmarXGuiComponentPlugins  # For RemoteGui plugin.
        # RobotAPI
        ## RobotAPICore
        ## RobotAPIInterfaces
        ## RobotAPIComponentPlugins  # For ArViz and other plugins.
        core_interfaces
    # DEPENDENCIES_LEGACY
        ## Add libraries that do not provide any targets but ${FOO_*} variables.
        # FOO
    # If you need a separate shared component library you can enable it with the following flag.
    # SHARED_COMPONENT_LIBRARY
)
